<?php

namespace Unify\CpanelWhm\Exceptions;

use Unify\CpanelWhm\Exceptions\Exceptions;

class EmptyInstanceException extends Exception
{
    /**
     * Create a new exception instance.
     *
     * @return void
     */
    public function __construct($message="Bad initialization, Instance Not Set", $code=401) {
        parent::__construct($message, $code);
    }
}