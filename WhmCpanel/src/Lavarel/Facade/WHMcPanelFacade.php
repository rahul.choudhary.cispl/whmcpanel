<?php

namespace Unify\CpanelWhm\Laravel\Facade;

use Illuminate\Support\Facades\Facade;

class WHMcPanelFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'WhmCpanel';
    }
}